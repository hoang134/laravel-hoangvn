<?php
namespace App\Repositories\Product;

use App\Models\Product;
use App\Repositories\Repository;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{
    //lấy model tương ứng
    public function getModel()
    {
        return Product::class;
    }
}
