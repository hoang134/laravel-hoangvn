<?php
namespace App\Repositories;
use App\Models\Blog;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository implements RepositoryInterface
{
    //model muốn tương tác
    protected $model;

   //khởi tạo
    public function __construct()
    {
        $this->setModel();
    }

    //lấy model tương ứng
    abstract public function getModel();


    public function setModel()
    {
        $this->model = app()->make(
            $this->getModel()
        );
    }

     /**
     * Get all
     * @return mixed
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Get one
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->model->find($id);
    }
    //
    // public function create($attributes = [])
    // {
    //     return $article = $this->model::create($attributes);
    // }

     /**
     * Store
     * @param array $attributes
     * @return mixed
     */
    public function store($attributes = [])
    {
        return $article = $this->model::create($attributes);
    }

    /**
     * Update
     * @param $id
     * @param array $attributes
     * @return mixed
     */
    public function update($id,)
    {
        $article = $this->model->update($request->all());
        return $article;
    }

    /**
     * Delete
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model::destroy($id);
    }
}
