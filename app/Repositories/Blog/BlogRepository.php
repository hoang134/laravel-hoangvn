<?php

namespace App\Repositories\Blog;

use App\Models\Blog;
use App\Repositories\Repository;
class BlogRepository extends BaseRepository implements BlogRepositoryInterface
{
    public function getModel()
    {
        return Blog::class;
    }
}
