<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Http\Resources\PostResurce;
use App\Models\Article;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function create(PostRequest $request)
    {
        $post = Post::create($request->all());
        return response()->json(new PostResurce($post), 201);
    }

    public function updatePost(PostRequest $request)
    {
        $post = Post::query()->findOrFail($request->id);
        $post->title = $request->title;
        $post->body = $request->body;
        $post->save();
    }

    public function deletePost($id)
    {
        $post = Post::query()->firstOrFail($id);
        $post->delete();
    }

    public function getPost($id)
    {
        $post = Post::query()->firstOrFail($id);
        return $post;
    }

    public function searchPost(PostRequest $request)
    {
        $post = Post::query()->where('title','like','%'.$request->title.'%')
        ->orWhere('body','body','%'.$request->body.'%');
        return $post;
    }
}
