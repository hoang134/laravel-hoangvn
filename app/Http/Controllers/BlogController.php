<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlogRequest;
use App\Http\Resources\BlogResurce;
use App\Http\Resources\PostResurce;
use App\Models\Blog;
use App\Repositories\Blog\BlogRepositoryInterface;
use App\Repositories\Product\ProductRepositoryInterface;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    protected $blogRepo;

    public function __construct(BlogRepositoryInterface $blogRepo)
    {
        $this->blogRepo = $blogRepo;
    }

    public function index(Request $request)
    {
        return $this->blogRepo->getAll();
    }

    /**
     * Get one
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
       $blog =  $this->blogRepo->find($id);
        return response()->json($blog, 200);
    }

    /**
     * Update
     * @param $id
     * @param array $blogs
     * @return mixed
     */
    public function update(BlogRequest $request, $id)
    {
        $blogs = $this->blogRepo->update($request->all());
        return response()->json(new PostResurce($blogs), 200);
    }

    /**
     * Store
     * @param array $blogs
     * @return mixed
     */
    public function store(BlogRequest $request)
    {
        $blogs = $this->blogRepo->store($request->all());
        return response()->json(new BlogResurce($blogs), 201);
    }

    /**
     * Delete
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $this->blogRepo->delete($id);
        return response()->json(null, 204);
    }
}
