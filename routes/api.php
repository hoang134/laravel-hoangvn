<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Article;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\BlogController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//Route::prefix('articles')->namespace('article.')->group(function () {
//   Route::get('',[ArticleController::class,'index'])->name('index');
//   Route::get('/{article}',[ArticleController::class,'show'])->name('show');
//   Route::post('store',[ArticleController::class,'store'])->name('store');
//   Route::put('update/{article}',[ArticleController::class,'update'])->name('update');
//   Route::delete('delete/{article}',[ArticleController::class,'delete'])->name('delete');
//});

// Route::prefix('post')->namespace('post.')->group(function () {
//     Route::get('',[PostController::class,'index'])->name('index');
//     Route::get('create',[PostController::class,'createPost'])->name('show');
//     Route::post('store',[PostController::class,'store'])->name('store');
//     Route::put('update/{id}',[PostController::class,'update'])->name('update');
//     Route::delete('delete/{id}',[PostController::class,'delete'])->name('delete');
// });

//Route::prefix('blog')->namespace('blog.')->group(function () {
//    Route::get('',[BlogController::class,'index'])->name('index');
//    Route::get('create',[BlogController::class,'create'])->name('create');
//    Route::post('store',[BlogController::class,'store'])->name('store');
//    Route::put('update/{id}',[BlogController::class,'update'])->name('update');
//    Route::delete('delete/{id}',[BlogController::class,'delete'])->name('delete');
//});
// Route::post('store',[BlogController::class,'store']);

Route::resource('blogs', BlogController::class);

