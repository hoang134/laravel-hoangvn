<?php

namespace Database\Seeders;

use App\Models\Blog;
use Illuminate\Database\Seeder;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $blog = new Blog();
       $blog->id = 1;
       $blog->title = 'title1';
       $blog->body = 'body1';
       $blog->save();

       $blog = new Blog();
       $blog->id = 2;
       $blog->title = 'title2';
       $blog->body = 'body2';
       $blog->save();

       $blog = new Blog();
       $blog->id = 3;
       $blog->title = 'title3';
       $blog->body = 'body3';
       $blog->save();
    }
}
