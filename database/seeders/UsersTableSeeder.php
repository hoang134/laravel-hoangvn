<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\Fluent\Concerns\Has;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->id = 1;
        $user->name = 'hoang';
        $user->email = 'hoang@gmail.com';
        $user->password = Hash::make('123456');
        $user->save();

        $user = new User();
        $user->id = 2;
        $user->name = 'hoang2';
        $user->email = 'hoang2@gmail.com';
        $user->password = Hash::make('123456');
        $user->save();
    }
}
