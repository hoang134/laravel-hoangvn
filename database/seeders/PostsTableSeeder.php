<?php

namespace Database\Seeders;

use App\Models\Post;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $post = new Post();
        $post->title = 'title1';
        $post->body = 'body 1';
        $post->save();

        $post = new Post();
        $post->title = 'post title 2';
        $post->body = 'body 2';
        $post->save();

        $post = new Post();
        $post->title = 'post title 3';
        $post->body = 'body 3';
        $post->save();
    }
}
