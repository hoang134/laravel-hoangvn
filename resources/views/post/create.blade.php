<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>post</title>
</head>
<body>
    <div class="d-flex m-3">
        <h3 class='m-auto'>Create Post</h3>
    </div>
    <div class="container">
        <form>
            <div class="mb-3">
                <label for="title" class="form-label">Title</label>
                <input type="email" class="form-control" id="title" name = "title">
            </div>
            <div class="form-floating"> 
            <textarea class="form-control" placeholder="Content" id="Content"></textarea>
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1">
            </div>
            <div class="mb-3 form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck2">
                <label class="form-check-label" for="exampleCheck1">user_id_1</label>
            </div>
            <div class="mb-3 form-check">
                <input value=1 type="checkbox" class="form-check-input" id="userIdCheck2">
                <label class="form-check-label" for="exampleCheck2">use_id_2</label>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</html>